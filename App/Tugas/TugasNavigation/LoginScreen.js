import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, ScrollView, Dimensions, Button } from 'react-native';

export default function App() {

  return (
      <View style={styles.container}>
        <ScrollView>
          <Image style={styles.logo} source={require('../Tugas13/assets/logo.png')} />
          <Text style={{fontSize: 24,
                        lineHeight: 28,
                        color: '#003366',
                        textAlign: 'center',
                        marginTop: 70}}>Login</Text>
          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelInput}>Username/Email</Text>
              <TextInput style={styles.inputBox} />
            </View>
            <View style={{marginTop: 16}}>
              <Text style={styles.labelInput}>Password</Text>
              <TextInput style={styles.inputBox} />
            </View>
          </View>
          <View style={styles.buttonDaftar}>
            <Text style={{fontSize: 24, color: 'white'}}>Masuk</Text>
          </View>
          <Text style={{marginVertical: 16,
                        textAlign: 'center',
                        fontSize: 24,
                        color: '#3EC6FF'}}>atau</Text>
          <View style={styles.buttonMasuk}>
            <Text style={{fontSize: 24, color: 'white'}}>Daftar</Text>
          </View>
        </ScrollView>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: Dimensions.get('window').height,
    backgroundColor: 'white'
  },
  logo: {
    width: '100%'
  },
  inputContainer: {
    paddingHorizontal: 40,
    marginVertical: 40
  },
  labelInput: {
    fontSize: 12,
    lineHeight: 14,
    color: '#003366'
  },
  inputBox: {
    height: 48,
    width: '100%',
    borderColor: '#003366',
    borderWidth: 1,
    marginTop: 4
  },
  buttonDaftar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    width: 140,
    height: 40,
    backgroundColor: '#3EC6FF',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  buttonMasuk: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    width: 140,
    height: 40,
    backgroundColor: '#003366',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 77
  }
});
