// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
];

readBooks (time = 10000, books[i = 0], params = (param) => {
    if (i < books.length - 1) {
        i++;
        readBooks(param, books[i], params);
    }
});