var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
];

var i = 0;

function readBook (sisa = 10000) {
    if (i >= books.length) {
    } else {
        readBooksPromise(sisa, books[i])
            .then(function (sisaWaktu) {
                i++;
                readBook(sisaWaktu);
            })
            .catch (function (sisaWaktu) {
                i++;
                readBook(sisaWaktu);
            })
    }
}

readBook();