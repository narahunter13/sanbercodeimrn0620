//Nomor 1
function arrayToObject(arr) {
    var obj = {};
    var now = new Date();
    var thisYear = now.getFullYear();
    for (var i = 0; i < arr.length; i++) {
        obj[arr[i][0] + arr[i][1]] = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: ((thisYear - arr[i][3]) < 0 || arr[i][3] == null) ? "Invalid Birth Year" : (thisYear - arr[i][3])
        }
        console.log(i + 1 + ". " + obj[arr[i][0] + arr[i][1]].firstName + " " + obj[arr[i][0] + arr[i][1]].lastName + " :", obj[arr[i][0] + arr[i][1]]);
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]); // ""



//Nomor 2
function shoppingTime(memberId, money) {
    //Termahal -> Termurah
    var barang = [
                    ["Sepatu Stacattu", 1500000],
                    ["Baju Zoro", 500000],
                    ["Baju H & N", 250000],
                    ["Sweater Uniklooh", 175000],
                    ["Casing Handphone", 50000]
                 ]
    if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else if (memberId == "" || memberId == undefined) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        var list = [];
        var change = money;
        for (var i = 0; i < barang.length; i++) {
            if (change >= barang[i][1]) {
                list.push(barang[i][0]);
                change -= barang[i][1];
            }
        }
        var transaksi = {
            memberId : memberId,
            money : money,
            listPurchased : list,
            changeMoney : change
        };
        return transaksi;
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Nomor 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var hasil = [];
    for (var i = 0; i < arrPenumpang.length; i++) {
        var awal = 0;
        var akhir = 0;
        var obj = {};
        obj.penumpang = arrPenumpang[i][0];
        obj.naikDari = arrPenumpang[i][1];
        obj.tujuan = arrPenumpang[i][2];
        for (var j = 0; j < rute.length; j++) {
            if (rute[j] === obj.naikDari) {
                awal = j;
            }
            if (rute[j] === obj.tujuan) {
                akhir = j;
            }
        }
        obj.bayar = (akhir - awal) * 2000;
        hasil.push(obj);
    }
    return hasil;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]