//Nomor 1
var i = 0;
var maju = true;
while (i <= 20) {
    if (maju) {
        if (i==0) {
            console.log('LOOPING PERTAMA');
            i += 2;
        }
        console.log(i + ' - I love coding');
        if (i == 20) {
            maju = false;
        } else {
            i += 2;
        }
    }  
    
    if (!maju) {
        if (i == 20) {
            console.log('LOOPING KEDUA');
        }
        console.log(i + ' - I will become a mobile developer');
        if (i == 2) {
            break;
        } else {
            i -= 2;
        }
    }
}


//Nomor 2
for (var i = 1; i <= 20; i++) {
    if (i % 2 == 0) {
        console.log(i + ' - Berkualitas');
    } else if (i % 2 != 0) {
        if (i % 3 == 0) {
            console.log(i + ' - I Love Coding ');
        } else {
            console.log(i + ' - Santai');
        }
    } else if ((i % 2 != 0) && (i % 3 == 0)) {
        console.log(i + ' - I Love Coding ');
    }
}

//Nomor 3
for (var baris = 1; baris<=4; baris++) {
    for (var kolom = 1; kolom <= 8; kolom++) {
        if (kolom == 8) {
            process.stdout.write('#\n');
        } else {
            process.stdout.write('#');
        }
    }
}

//Nomor 4
for (var baris = 1; baris <= 7; baris++) {
    for (var kolom = 1; kolom <= baris; kolom++) {
        if (kolom == baris) {
            process.stdout.write('#\n');
        } else {
            process.stdout.write('#');
        }
    }
}

//Nomor 5
for (var baris = 1; baris <= 8; baris++) {
    for (var kolom = 1; kolom <= 8; kolom++) {
        if (baris % 2 == 0) {
            if (kolom % 2 == 0) {
                if (kolom == 8) {
                    process.stdout.write(' \n');
                } else {
                    process.stdout.write(' ');
                }
            } else {
                if (kolom == 8) {
                    process.stdout.write('#\n');
                } else {
                    process.stdout.write('#');
                }
            }
        } else {
            if (kolom % 2 == 0) {
                if (kolom == 8) {
                    process.stdout.write('#\n');
                } else {
                    process.stdout.write('#');
                }
            } else {
                if (kolom == 8) {
                    process.stdout.write(' \n');
                } else {
                    process.stdout.write(' ');
                }
            }
        }
    }
}