//Nomor 1
function range (startNum, finishNum) {
    var array = [];
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else {
        if (startNum <= finishNum) {
            for (var i = startNum; i <= finishNum; i++) {
                array.push(i);
            }
        } else {
            for (var i = startNum; i >= finishNum; i--) {
                array.push(i);
            }
        }

        return array;
    }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

//Nomor 2
function rangeWithStep(startNum, finishNum, step) {
    var array = [];
    if (startNum == undefined || finishNum == undefined || step == undefined) {
        return -1;
    } else {
        if (startNum <= finishNum) {
            for (var i = startNum; i <= finishNum; i += step) {
                array.push(i);
            }
        } else {
            for (var i = startNum; i >= finishNum; i -= step) {
                array.push(i);
            }
        }

        return array;
    }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

//Nomor 3
function sum(startNum = 0, finishNum = 0, step = 1) {
    var array = [];
    if (startNum <= finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            array.push(i);
        }
    } else {
        for (var i = startNum; i >= finishNum; i -= step) {
            array.push(i);
        }
    }

    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        sum += array[i];
    }

    return sum;
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

//Nomor 4
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]
function dataHandling (input) {
    for (var i = 0; i < input.length; i++) {
        console.log("Nomor ID: " + input[i][0]);
        console.log("Nama Lengkap: " + input[i][1]);
        console.log("TTL: " + input[i][2] + input[i][3]);
        if (i == input.length - 1) {
            console.log("Hobi: " + input[i][4]);
        } else {
            console.log("Hobi: " + input[i][4] + "\n");
        }
    }
}

dataHandling(input);

//Nomor 5
function balikKata (input) {
    var reverseWord = "";
    for (var i = input.length - 1; i >= 0; i--) {
        reverseWord += input[i];
    }
    return reverseWord;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

//Nomor 6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2 (input) {
    //Menambah dan menghapus elemen
    input.splice(4, 4, "Pria", "SMA Internasional Metro");
    input.splice(1, 1, "Roman Alamsyah Elsharawy");
    input.splice(2, 1, "Provinsi Bandar Lampung");
    console.log(input);

    //Bulan
    var date = input[3].split('/');
    var bulan = Number(date[1]);
    switch (bulan) {
        case 1: { bulan = 'Januari'; break; }
        case 2: { bulan = 'Februari'; break; }
        case 3: { bulan =  'Maret'; break; }
        case 4: { bulan =  'April'; break; }
        case 5: { bulan =  'Mei'; break; }
        case 6: { bulan =  'Juni'; break; }
        case 7: { bulan =  'Juli'; break; }
        case 8: { bulan =  'Agustus'; break; }
        case 9: { bulan =  'September'; break; }
        case 10: { bulan =  'Oktober'; break; }
        case 11: { bulan =  'November'; break; }
        case 12: { bulan =  'Desember'; break; }
    }
    console.log(bulan);

    //Sorted date
    var sorted = date.slice(0);
    console.log(sorted.sort(function (value1, value2) { return value2 - value1 }));

    //Join date
    console.log(date.join('-'));

    //Max 15 chars
    console.log(input[1].slice(0, 14));
}
dataHandling2(input);